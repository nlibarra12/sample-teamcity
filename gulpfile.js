const gulp = require('gulp');

function defaultTask(cb) {

    var fs = require('fs');
    var dt = new Date();

    fs.appendFile('log.txt', dt.toString() + "\n" );

    cb();
  }

  exports.default = defaultTask